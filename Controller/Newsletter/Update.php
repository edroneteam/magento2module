<?php

/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Controller\Newsletter;

use Edrone\Magento2module\Helper\Config;
use Edrone\Magento2module\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Newsletter\Model\Subscriber;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Framework\App\Action\Context;

class Update extends Action {

    protected $dataHelper;
    protected $configHelper;
    protected $subscriberHelper;
    protected $subscriberFactory;

    public function __construct(
    Context $context, Data $dataHelper, Config $configHelper, Subscriber $subscriberHelper, SubscriberFactory $subscriberFactory
    ) {
        $this->dataHelper = $dataHelper;
        $this->configHelper = $configHelper;
        $this->subscriberHelper = $subscriberHelper;
        $this->subscriberFactory = $subscriberFactory;
        parent::__construct($context);
    }

    public function execute() {
        if (!$this->configHelper->isNewsletterSyncEnabled()) {
            $this->getResponse()->setBody('1');
            return;
        }

        $email = $this->getRequest()->getParam('email');
        $subscriber_status = $this->getRequest()->getParam('subscriber_status');
        $event_date = $this->getRequest()->getParam('event_date');
        $event_id = $this->getRequest()->getParam('event_id');
        $app_id = $this->getRequest()->getParam('app_id');
        $signature = $this->getRequest()->getParam('signature');

        if ($email && (isset($subscriber_status)) && $event_date && $event_id && $app_id && $signature && $this->dataHelper->validateToken($email, $subscriber_status, $event_date, $event_id, $signature)) {
            $subscriber = $this->subscriberHelper->loadByEmail($email);
            if (!$subscriber) {
                $this->subscriberFactory->create()->subscribe($email);
                $subscriber = $this->subscriberHelper->loadByEmail($email);
            }
            if ($subscriber_status == "0" || $subscriber_status == "") {
                $subscriber->unsubscribe();
                $this->getResponse()->setBody('0-U');
            } else if ($subscriber_status == "1") {
                $subscriber->subscribe($email);
                $subscriber->setStatus(1)->save();
                $this->getResponse()->setBody('0-S');
            }
            return;
        } else {
            $this->getResponse()->setBody('2');
            return;
        }
    }
}
