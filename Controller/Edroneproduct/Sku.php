<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Controller\Edroneproduct;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Exception\NoSuchEntityException;

class Sku extends Action
{
  protected $productRepositoryInterface;
  protected $configurable;
  protected $productHelper;
  protected $imageHelper;
  protected $categoryHelper;
  protected $categoryCollectionFactory;

  public function __construct(
      Context $context,
      ProductRepositoryInterface $productRepositoryInterface,
      Configurable $configurable,
      Product $productHelper,
      Image $imageHelper,
      CollectionFactory $categoryCollectionFactory
  ) {
      $this->productRepositoryInterface = $productRepositoryInterface;
      $this->configurable = $configurable;
      $this->productHelper = $productHelper;
      $this->imageHelper = $imageHelper;
      $this->categoryCollectionFactory = $categoryCollectionFactory;
      parent::__construct($context);
  }

  public function execute()
  {
      $productArray = [];
      $productArray['sku'] = '';
      $productArray['id'] = '';
      $productArray['title'] = '';
      $productArray['image'] = '';
      $productArray['product_url'] = '';
      $productArray['product_category_names'] = '';
      $productArray['product_category_ids']   = '';
      $productArray['product_base_price']   = '';
      $productArray['product_final_price'] = '';

      try {
        $sku = $this->getRequest()->getParam('v');
        $product   = $this->productRepositoryInterface->get($sku);
        $parentIds = $this->configurable->getParentIdsByChild($product->getId());

        if (count($parentIds) > 0) {
            $product = $this->productRepositoryInterface->getById($parentIds[0]);
        }
        $productArray['sku']         = $product->getSku();
        $productArray['id']          = $product->getId();
        $productArray['title']       = $product->getName();
        $productArray['image']       = $this->imageHelper->init(
            $product,
            'product_page_image_large'
        )->resize(438)->getUrl();
        $productArray['product_url'] = $product->getProductUrl();

        $categoryIds   = $product->getCategoryIds();
        $categoryNames = [];
        $categories = $this->categoryCollectionFactory->create()->addIdFilter($categoryIds)->addAttributeToSelect('name');
        foreach ($categories as $category) {
          $categoryNames[] = $category->getName();
        }
        $productArray['product_category_names'] = join('~', $categoryNames);
        $productArray['product_category_ids']   = join('~', $categoryIds);
        $productArray['product_base_price']   = $product->getPriceInfo()->getPrice('regular_price')->getValue();
        $productArray['product_final_price']  = $product->getPriceInfo()->getPrice('final_price')->getValue();
      } catch(NoSuchEntityException $e) {

      }
      $this->getResponse()->setHeader('Content-type', 'application/json');
      $this->getResponse()->setBody(json_encode($productArray));

  }
}
