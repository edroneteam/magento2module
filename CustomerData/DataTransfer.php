<?php

namespace Edrone\Magento2module\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Edrone\Magento2module\Model\Customer as EdroneCustomer;
use Edrone\Magento2module\Model\Category as EdroneCategory;
use Edrone\Magento2module\Model\Settings as EdroneSettings;
use Edrone\Magento2module\Model\Cart as EdroneCart;
use Edrone\Magento2module\Model\PrintNorm as EdronePrintNorm;

class DataTransfer implements SectionSourceInterface {

    /**
     * @var EdroneCustomer
     */
    protected $edroneCustomer;

    /**
     * @var EdroneSettings
     */
    protected $edroneSettings;

    /**
     * @var EdroneCart
     */
    protected $edroneCart;

    /**
     * @var EdroneCategory
     */
    private $edroneCategory;

    public function __construct(
    EdroneCustomer $edroneCustomer, EdroneSettings $edroneSettings, EdroneCart $edroneCart, EdroneCategory $edroneCategory
    ) {
        $this->edroneCustomer = $edroneCustomer;
        $this->edroneSettings = $edroneSettings;
        $this->edroneCart = $edroneCart;
        $this->edroneCategory = $edroneCategory;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData() {
        try {
            $data = [
                'error' => false,
                'customer' => EdronePrintNorm::norm($this->edroneCustomer->getCustomer()),
                'settings' => EdronePrintNorm::norm($this->edroneSettings->getSettings()),
                'cart' => EdronePrintNorm::norm($this->edroneCart->getCart()),
                'category' => EdronePrintNorm::norm($this->edroneCategory->getCategory())
            ];
        } catch (\Exception $e) {
            $data = [
                'error' => true,
            ];
        }
        return $data;
    }

}
