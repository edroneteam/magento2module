Magento2Module
License URI: https:/edrone.me/integration-license/


## Change Log
> 0.5.12
* fix minor issues with php 8.*
> 0.5.11
* Upgrade PHP version to 8.*
> 0.5.10
* added category_view
> 0.5.9
* fix add_to_cart trace sending even when there is product_view
> 0.5.8
* auto create subscriber if he don't exists
> 0.5.5
* fix system emails
> 0.5.4
* minor fixes
> 0.5.3
* added more info about categories visited
> 0.5.2
* scope_store
> 0.5.1
* multistore fix
> 0.4.8
* Integration license URI added
> 0.4.9
* check if function edrone.init exists before run
> 0.5.0
* check if edrone newsletter double opt-in is enabled
