<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Test\Integration;

use Magento\TestFramework\Backend\App\Config;
use Magento\TestFramework\ObjectManager;

class ConfigurationTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultValuesExists()
    {
        /** @var \Magento\TestFramework\Backend\App\Config $config */
        $config = ObjectManager::getInstance()->create(Config::class);
        $this->assertNotEmpty($config->getValue('edrone/Magento2module/app_id'));
        $this->assertNotEmpty($config->getValue('edrone/Magento2module/app_secret'));
        $this->assertNotEmpty($config->getValue('edrone/Magento2module/external_script_url'));
        $this->assertNotEmpty($config->getValue('edrone/Magento2module/collector_url'));
    }

}
