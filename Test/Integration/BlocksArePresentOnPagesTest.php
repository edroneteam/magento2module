<?php

namespace Edrone\Magento2module\Test\Integration;

use Magento\Checkout\Model\Session;
use Magento\Framework\View\LayoutInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\QuoteManagement;
use Magento\TestFramework\ObjectManager;

/**
 * @magentoDbIsolation enabled
 */
class BlocksArePresentOnPagesTest extends \Magento\TestFramework\TestCase\AbstractController
{
    public function testDefaultBlockExistsOnHomepage()
    {
        $this->dispatch('/');
        /** @var LayoutInterface $layout */
        $layout = ObjectManager::getInstance()->get(LayoutInterface::class);
        $block  = $layout->getBlock('edrone_Magento2module');

        $this->assertInstanceOf(\Edrone\Magento2module\Block\Magento2module::class, $block);
    }

    /**
     * @magentoDataFixture Magento/Catalog/_files/product_simple.php
     */
    public function testBlocksExistsOnProductPage()
    {
        $this->dispatch('catalog/product/view/id/1');
        /** @var LayoutInterface $layout */
        $layout = ObjectManager::getInstance()->get(LayoutInterface::class);

        $block1 = $layout->getBlock('edrone');
        $this->assertInstanceOf(\Edrone\Magento2module\Block\Product::class, $block1);

        $block2 = $layout->getBlock('edrone_js');
        $this->assertInstanceOf(\Magento\Framework\View\Element\Template::class, $block2);
    }

    /**
     * @magentoDataFixture Magento/Catalog/_files/category.php
     */
    public function testJsBlockExistsOnCategoryPage()
    {
        $this->dispatch('catalog/category/view/id/333');
        /** @var LayoutInterface $layout */
        $layout = ObjectManager::getInstance()->get(LayoutInterface::class);

        $block = $layout->getBlock('edrone_js');
        $this->assertInstanceOf(\Magento\Framework\View\Element\Template::class, $block);
    }

    public function testBlocksExistsOnAddAction()
    {
        $this->dispatch('edrone/edroneproduct/add');
        /** @var LayoutInterface $layout */
        $layout = ObjectManager::getInstance()->get(LayoutInterface::class);

        $block1 = $layout->getBlock('edrone_cart');
        $this->assertInstanceOf(\Edrone\Magento2module\Block\Cart::class, $block1);
    }

    /**
     * @magentoDataFixture Magento/Sales/_files/quote.php
     * @magentoDataFixture Magento/Sales/_files/order.php
     */
    public function testBlockExistsOnCheckoutSuccessPage()
    {
        $session = ObjectManager::getInstance()->get(Session::class);
        $session->setLastSuccessQuoteId(1);
        $session->setLastQuoteId(1);
        $session->setLastOrderId(1);
        $this->dispatch('checkout/onepage/success');
        /** @var LayoutInterface $layout */
        $layout = ObjectManager::getInstance()->get(LayoutInterface::class);
        $block  = $layout->getBlock('edrone');

        $this->assertInstanceOf(\Edrone\Magento2module\Block\Order::class, $block);
    }
}
