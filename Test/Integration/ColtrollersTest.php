<?php

namespace Edrone\Magento2module\Test\Integration;

/**
 * @magentoDbIsolation enabled
 * @magentoDataFixture Magento/Catalog/_files/product_simple.php
 */
class Controllers extends \Magento\TestFramework\TestCase\AbstractController
{
    public function testIdAction()
    {
        $this->getRequest()->setParam('v', 1);
        $this->dispatch('edrone/edroneproduct/id');

        $response = $this->getResponse()->getBody();
        $response = json_decode($response);
        $this->assertNotEmpty($response);
    }

    public function testSkuAction()
    {
        $this->getRequest()->setParam('v', 'simple');
        $this->dispatch('edrone/edroneproduct/sku');

        $response = $this->getResponse()->getBody();
        $response = json_decode($response);
        $this->assertNotEmpty($response);
    }

    public function testAddAction()
    {
        $this->dispatch('edrone/edroneproduct/add');

        $this->assertNotEquals('noroute', $this->getRequest()->getControllerName());
    }

    public function testUpdateAction()
    {
        $this->dispatch('edrone/newsletter/update');

        $this->assertNotEquals('noroute', $this->getRequest()->getControllerName());
    }
}
