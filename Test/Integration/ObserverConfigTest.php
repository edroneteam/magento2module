<?php

namespace Edrone\Magento2module\Test\Integration;

use Magento\Framework\Event\ConfigInterface;
use Magento\TestFramework\ObjectManager;
use Edrone\Magento2module\Observer\AddToCart;
use Edrone\Magento2module\Observer\ExportNewOrder;

class ObserverConfigTest extends \PHPUnit_Framework_TestCase
{
    public function testAddToCartObserverConfig()
    {
        $event = 'checkout_cart_product_add_after';

        /** @var ConfigInterface $eventConfig */
        $eventConfig = ObjectManager::getInstance()->create(ConfigInterface::class);

        $observers = $eventConfig->getObservers($event);
        $this->assertArrayHasKey('edrone_checkout_cart_product_add_after', $observers);
        $this->assertSame(AddToCart::class, $observers['edrone_checkout_cart_product_add_after']['instance']);
    }

    public function testExportNewOrderObserverConfig()
    {
        $event = 'sales_order_place_after';

        /** @var ConfigInterface $eventConfig */
        $eventConfig = ObjectManager::getInstance()->create(ConfigInterface::class);

        $observers = $eventConfig->getObservers($event);
        $this->assertArrayHasKey('edrone_sales_order_place_after', $observers);
        $this->assertSame(ExportNewOrder::class, $observers['edrone_sales_order_place_after']['instance']);
    }
}
