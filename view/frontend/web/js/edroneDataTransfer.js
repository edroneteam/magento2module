define([
    'Magento_Customer/js/customer-data',
    'jquery',
], function(customerData, $){

    'use strict';
    var sections = ['edrone-datatransfer'];
    customerData.invalidate(sections);
    customerData.reload(sections, true);
            
    return function (options) {
        var dataObject = customerData.get("edrone-datatransfer");
        dataObject.subscribe(function (_dataObject) {
            window._edroneDataTransfer.execute(_dataObject);
        }, this);
    }

});