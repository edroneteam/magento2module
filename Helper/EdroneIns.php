<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ProductMetadataInterface;
use Edrone\Magento2module\Helper\Config;
use Edrone\Magento2module\Helper\Data;

class EdroneIns extends AbstractHelper
{
    /** @var string AppId */
    private $appid = '';
    /** @var string $secret Secret */
    private $secret = '';
    /** @var string $version platformVersion */
    private $version = '';
    /** @var string $platformVersion platformVersion */
    private $platformVersion = '';
    /** @var string Trace url. def https://api.edrone.me/trace */
    private $trace_url = 'https://api.edrone.me/trace.php';
    /** @var array  Prepared request array */
    private $preparedpack = array();
    /** @var \Closure Closure method called on error */
    private $errorHandle = null;
    /** @var \Closure Closure method called on success */
    private $readyHandle = null;
    /** @var array Last request information */
    private $lastRequest = null;


    public function __construct(Config $config, Data $data, ProductMetadataInterface $productMetadata) {
        $this->appid = $config->getAppId();
        $this->version = $data->getExtensionVersion();
        $this->platformVersion = $productMetadata->getVersion();
    }
    /**
     * @param string $appId
     *
     * @return $this
     */
    public function setAppId($appId)
    {
        $this->appid = $appId;

        return $this;
    }

    /**
     * @param string $secret
     *
     * @return $this
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;

        return $this;
    }

    /**
     * @param string $version
     *
     * @return $this
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @param string $platformVersion
     *
     * @return $this
     */
    public function setPlatformVersion($platformVersion)
    {
        $this->platformVersion = $platformVersion;

        return $this;
    }

    /**
     * @param string $traceUrl
     *
     * @return $this
     */
    public function setTraceUrl($traceUrl = 'https://api.edrone.me/trace.php')
    {
        $this->trace_url = $traceUrl;

        return $this;
    }

    /**
     *
     * Prepare event to send
     *
     * @param EdroneEvent $event Use object of EdroneEventAddToCart,EdroneEventOrder,EdroneEventOrder,EdroneEventOther
     *
     * @return EdroneIns
     * @since 1.0.0
     */
    public function prepare($event)
    {
        if (!($event instanceof EdroneEvent)) {
            $this->preparedpack = array();
            throw new \Exception('Event must by child EdroneEvent class ');
        }
        $event->pre_init();
        $event->init();
        $this->preparedpack = array_merge(
            $event->get(),
            array(
                "app_id"      => $this->appid,
                "platform"    => "magento2",
                "platform_version" => $this->platformVersion,
                "version"     => $this->version,
                "sender_type" => 'server',
            )
        );
        //Calc sign - beta
        ksort($this->preparedpack);
        $sign = '';
        foreach ($this->preparedpack as $key => $value) {
            $sign .= $value;
        }
        $this->preparedpack['sign'] = md5($this->secret . $sign);

        //
        return $this;
    }

    /**
     *
     * Force send prepared data
     * @since 1.0.0
     */
    public function send()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->trace_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->preparedpack));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        $data     = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $report   = curl_getinfo($ch);
        curl_close($ch);
        $this->lastRequest = array(
            "code"     => $httpcode,
            "response" => $data,
            "info"     => $report,
            'fields'   => $this->preparedpack,
        );
        if (($httpcode !== 200) && ($this->errorHandle !== null)) {
            call_user_func_array($this->errorHandle, array($this));
        } elseif (($httpcode === 200) && ($this->readyHandle !== null)) {
            call_user_func_array($this->readyHandle, array($this));
        }
    }

    /**
     * Return last request as array (debug)
     * @return array
     * @since 1.0.0
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    /**
     * Set Callbacks for error action and ready action
     *
     * @param \Closure $errorHandle
     * @param \Closure $readyHandle
     *
     * @since 1.0.0
     */
    public function setCallbacks($errorHandle = null, $readyHandle = null)
    {
        $this->errorHandle = $errorHandle;
        $this->readyHandle = $readyHandle;
    }
}
