<?php

/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\ModuleListInterface;

class Data extends AbstractHelper {

    /** @var Config */
    protected $_configHelper;
    protected $_moduleList;

    public function __construct(Context $context, Config $configHelper, ModuleListInterface $moduleList) {
        $this->_configHelper = $configHelper;
        $this->_moduleList = $moduleList;
        parent::__construct($context);
    }

    /**
     * @param string $value
     * @param string $token
     *
     * @return bool
     */
    public function validateToken($email, $subscriber_status, $event_date, $event_id, $token) {
        $appId = $this->_configHelper->getAppId();
        $appSecret = $this->_configHelper->getAppSecret();
        $hash = base64_encode(hash('sha256', $email . $subscriber_status . $event_date . $event_id . $appSecret . $appId));

        if (($hash == $token)) {
            return true;
        }

        return false;
    }

    /**
     *
     * @return string
     */
    public function utcNow() {
        $t = microtime(true);
        $micro = sprintf("%03d", ($t - floor($t)) * 1000000);

        return gmdate('Y-m-d\TH:i:s.', (int)$t) . $micro . 'Z';
    }

    /**
     *
     * @return string
     */
    public function getExtensionVersion() {
        $moduleInfo = $this->_moduleList->getOne('Edrone_Magento2module');
        return $moduleInfo['setup_version'];
    }

}
