<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Config extends AbstractHelper
{
    const APP_ID_CONFIG_PATH              = "edrone/Magento2module/app_id";
    const APP_SECRET_CONFIG_PATH          = "edrone/Magento2module/app_secret";
    const EXTERNAL_SCRIPT_URL_CONFIG_PATH = "edrone/Magento2module/external_script_url";
    const COLLECTOR_URL_CONFIG_PATH       = "edrone/Magento2module/collector_url";
    const NEWSLETTER_SYNC_ENABLED_PATH    = "edrone/newsletter/subscription_sync_enabled";
    const SERVERSIDE_ORDER_ENABLED_PATH   = "edrone/Magento2module/serverside_order";
    const EXTERNAL_DOUBLE_OPTIN           = "edrone/Magento2module/edrone_optin";
    const ADD_TO_CART                     = "edrone/Magento2module/add_to_cart";

    /**
     * @return string
     */
    public function getAppId()
    {
        return (string) $this->scopeConfig->getValue(self::APP_ID_CONFIG_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getAppSecret()
    {
        return (string) $this->scopeConfig->getValue(self::APP_SECRET_CONFIG_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getExternalScriptUrl()
    {
        return (string) $this->scopeConfig->getValue(self::EXTERNAL_SCRIPT_URL_CONFIG_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getCollectorUrl()
    {
        return (string) $this->scopeConfig->getValue(self::COLLECTOR_URL_CONFIG_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return bool
     */
    public function getNewsletterSubscriptionMailEnabled()
    {
        return $this->scopeConfig->getValue(self::EXTERNAL_DOUBLE_OPTIN, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return bool
     */
    public function isNewsletterSyncEnabled()
    {
        return $this->scopeConfig->getValue(self::NEWSLETTER_SYNC_ENABLED_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return bool
     */
    public function isServerSideOrderEnabled()
    {
        return $this->scopeConfig->getValue(self::SERVERSIDE_ORDER_ENABLED_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return bool
     */
    public function isAddToCartEnabled()
    {
        return $this->scopeConfig->getValue(self::ADD_TO_CART, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
