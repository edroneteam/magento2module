<?php

/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Helper;

class EdroneEventSubscribe extends EdroneEvent {

    /**
     * @return $this
     */
    public static function create() {
        return new self();
    }

    public function init() {
        $this->field['action_type'] = 'subscribe';
        $this->field['customer_tags'] = 'From PopUp';
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userCid($value) {
        parent::userCid($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userEmail($value) {
        parent::userEmail($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userFirstName($value) {
        parent::userFirstName($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userLastName($value) {
        parent::userLastName($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userSubscriberStatus($value) {
        parent::userSubscriberStatus($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userCountry($value) {
        parent::userCountry($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userCity($value) {
        parent::userCity($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userPhone($value) {
        parent::userPhone($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userTag($value) {
        parent::userTag($value);

        return $this;
    }

}
