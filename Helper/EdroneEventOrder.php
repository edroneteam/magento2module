<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Helper;

class EdroneEventOrder extends EdroneEvent
{

    public function init()
    {
        $this->field['action_type'] = 'order';
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userCid($value)
    {
        parent::userCid($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userEmail($value)
    {
        parent::userEmail($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userFirstName($value)
    {
        parent::userFirstName($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userLastName($value)
    {
        parent::userLastName($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userSubscriberStatus($value)
    {
        parent::userSubscriberStatus($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userCountry($value)
    {
        parent::userCountry($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userCity($value)
    {
        parent::userCity($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userPhone($value)
    {
        parent::userPhone($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function userTag($value)
    {
        parent::userTag($value);

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return  $this Description
     */
    public function productSkus($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_skus'] = $value;

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function productIds($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_ids'] = $value;

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function productTitles($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_titles'] = $value;

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function productImages($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_images'] = $value;

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function productUrls($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_urls'] = $value;

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function productCounts($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_counts'] = $value;

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function productCategoryIds($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_category_ids'] = $value;

        return $this;
    }

    /**
     *
     * @param type $value
     *
     * @return $this
     */
    public function productCategoryNames($value)
    {
        if (is_array($value)) {
            $value = implode('|', $value);
        }
        $this->field['product_category_names'] = $value;

        return $this;
    }

    public function orderId($value)
    {
        $this->field['order_id'] = $value;

        return $this;
    }

    public function orderPaymentValue($value)
    {
        $this->field['order_payment_value'] = $value;

        return $this;
    }

    public function orderBasePaymentValue($value)
    {
        $this->field['base_payment_value'] = $value;

        return $this;
    }

    public function orderDetails($value)
    {
        $this->field['order_details'] = $value;

        return $this;
    }

    public function orderCurrency($value)
    {
        $this->field['order_currency'] = $value;

        return $this;
    }

    public function orderBaseCurrency($value)
    {
        $this->field['base_currency'] = $value;

        return $this;
    }
}
