<?php
namespace Edrone\Magento2module\Helper;

use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\Product;

/**
 * Description of ProductHelper
 *
 * @author abialozyt
 */

class ProductHelper {

    private $product;

    private $categoryCollectionFactory;

    private $imageHelper;


    public function __construct(
      Product $product,
      CollectionFactory $categoryCollectionFactory,
      Image $imageHelper
    ) {
        $this->product = $product;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->imageHelper = $imageHelper;
    }
    /**
     * @return Category
     */
    public function getCategoryModel()
    {
        return $this->categoryModel;
    }

    /**
     * @return Image
     */
    public function getImageHelper()
    {
        return $this->imageHelper;
    }

   public function getProductData($product)
   {
        $productArray['sku']         = $product->getSku();
        $productArray['id']          = $product->getId();
        $productArray['title']       = $product->getName();
        $productArray['image']       = $this->getImageHelper()->init($product, 'product_page_image_large')->resize(438)
                                            ->getUrl();
        $productArray['product_url'] = $product->getProductUrl();

        $categoryIds   = $product->getCategoryIds();
        $categoryNames = [];
        $categories = $this->categoryCollectionFactory->create()->addIdFilter($categoryIds)->addAttributeToSelect('name');
        foreach ($categories as $category) {
          $categoryNames[] = $category->getName();
        }
        $productArray['product_category_names'] = join('~', $categoryNames);
        $productArray['product_category_ids']   = join('~', $categoryIds);

        return $productArray;
    }
}
