<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Block;

class Product extends Magento2module
{
    /**
     * @return bool
     */
    public function hasProductAddedToCart()
    {
        $hasBeenAdded = (bool) $this->checkoutSession->getProductHasBeenAdded();
        if ($hasBeenAdded) {
            $this->checkoutSession->unsProductHasBeenAdded();
        }
        return $hasBeenAdded;
    }

    /**
     * @return array
     */
    public function getProductData()
    {
        $productArray = array();
        $product = $this->registry->registry('current_product');

        $productArray['sku']         = $product->getSku();
        $productArray['id']          = $product->getId();
        $productArray['title']       = $product->getName();
        $productArray['image']       = $this->getImageHelper()->init($product, 'product_page_image_large')->resize(438)
                                            ->getUrl();
        $productArray['product_url'] = $product->getProductUrl();

        $categoryIds   = $product->getCategoryIds();
        $categoryNames = [];
        $categories = $this->getCategoryCollection($categoryIds);
        foreach ($categories as $category) {
          $categoryNames[] = $category->getName();
        }
        $productArray['product_category_names'] = join('~', $categoryNames);
        $productArray['product_category_ids']   = join('~', $categoryIds);

        return $productArray;
    }
}
