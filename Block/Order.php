<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Block;

use Edrone\Magento2module\Helper\EdroneEventOrder;

class Order extends Magento2module
{
    public function sendDataToServer($orderData, $customerData)
    {
        if (!$this->configHelper->isServerSideOrderEnabled()) {
            return;
        }
        try {
            $configHelper  = $this->configHelper;
            /** @var EdroneIns $edrone */
            $edrone = $this->getEdroneIns();
            $edrone->setAppId($configHelper->getAppId());
            $edrone->setSecret('');
            $edrone->setCallbacks(
                function ($obj) {
                    error_log("EDRONEPHPSDK ERROR - wrong request:" . json_encode($obj->getLastRequest()));
                },
                function () {
                }
            );
            $edrone->prepare(
              $this->getEdroneEventOrder()
                    ->userFirstName($customerData['first_name'])
                    ->userLastName($customerData['last_name'])
                    ->userCity($customerData['city'])
                    ->userCountry($customerData['country'])
                    ->userEmail($customerData['email'])
                    ->userPhone($customerData['phone'])
                    ->productSkus($orderData['sku'])
                    ->productTitles($orderData['title'])
                    ->productImages($orderData['image'])
                    ->productUrls($orderData['product_urls'])
                    ->productCategoryIds($orderData['product_category_ids'])
                    ->productCategoryNames($orderData['product_category_names'])
                    ->orderId($orderData['order_id'])
                    ->orderPaymentValue($orderData['order_payment_value'])
                    ->orderBasePaymentValue($orderData['base_payment_value'])
                    ->orderCurrency($orderData['order_currency'])
                    ->orderBaseCurrency($orderData['base_currency'])
                    ->productCounts($orderData['product_counts'])
            )->send();
        } catch (\Exception $e) {
            error_log("EDRONEPHPSDK ERROR:" . $e->getMessage() . ' more :' . json_encode($e));
        }
    }

    /**
     * @return array
     */
    public function getOrderData()
    {

        $orderData = $skus = $titles = $images = array();

        $lastOrderId = $this->checkoutSession->getLastRealOrderId();
        /** @var OrderModel $order */
        $order = $this->getOrderModel()->loadByIncrementId($lastOrderId);


        $product_category_names = array();
        $product_category_ids   = array();
        $product_counts         = array();
        $ids                    = array();
        $productUrls            = array();
        foreach ($order->getAllVisibleItems() as $item) {
            $categoryIds      = [];
            $categoryNames    = [];
            $product          = $item->getProduct();
            $skus[]           = $product->getSku();
            $ids[]            = $product->getId();
            $titles[]         = $product->getName();
            $product_counts[] = (int) $item->getQtyOrdered();
            $images[]         = ($product) ? (string) $this->getImageHelper()->init(
                $product,
                'product_page_image_large'
            )->resize(438)->getUrl() : '';

            $productUrls[]    = $product->getProductUrl();
            $categoryIds   = $product->getCategoryIds();
            $categoryNames = [];
            $categories = $this->getCategoryCollection($categoryIds);
            foreach ($categories as $category) {
              $categoryNames[] = $category->getName();
            }
            $product_category_names[] = join('~', $categoryNames);
            $product_category_ids[]   = join('~', $categoryIds);
        }

        $orderData['sku']                    = join('|', $skus);
        $orderData['id']                     = join('|', $ids);
        $orderData['title']                  = join('|', $titles);
        $orderData['image']                  = join('|', $images);
        $orderData['order_id']               = $order->getIncrementId();
        $orderData['order_payment_value']    = $order->getGrandTotal();
        $orderData['base_payment_value']     = $order->getBaseGrandTotal();
        $orderData['base_currency']          = $order->getBaseCurrencyCode();
        $orderData['order_currency']         = $order->getOrderCurrencyCode();
        $orderData['coupon']                 = $order->getCouponCode();
        $orderData['product_category_names'] = join('|', $product_category_names);
        $orderData['product_category_ids']   = join('|', $product_category_ids);
        $orderData['product_urls']           = join('|', $productUrls);
        $orderData['product_counts']         = join('|', $product_counts);

        return $orderData;
    }

    public function getCustomerData()
    {
        parent::getCustomerData();
        $lastOrderId = $this->checkoutSession->getLastRealOrderId();
        /** @var OrderModel $order */
        $order = $this->getOrderModel()->loadByIncrementId($lastOrderId);

        $countryFactory = $this->getCountryFactory();
        $this->customerData['first_name'] = $order->getBillingAddress()->getFirstname();
        $this->customerData['last_name']  = $order->getBillingAddress()->getLastname();
        $this->customerData['email']      = $order->getBillingAddress()->getEmail();
        $this->customerData['country']    = $countryFactory->create()->loadByCode(
            $order->getBillingAddress()->getCountryId()
        )->getName();
        $this->customerData['city']       = $order->getBillingAddress()->getCity();
        $this->customerData['phone']      = $order->getBillingAddress()->getTelephone();

        return $this->customerData;
    }
}
