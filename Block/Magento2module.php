<?php
/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Block;

use Edrone\Magento2module\Helper\Config;
use Edrone\Magento2module\Helper\Data;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Model\Product;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\View\Element\Template;
use Magento\Newsletter\Model\Subscriber;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Registry;
use Magento\Sales\Model\Order;
use Magento\Directory\Model\CountryFactory;
use Edrone\Magento2module\Helper\EdroneIns;
use Edrone\Magento2module\Helper\EdroneEventOrder;

class Magento2module extends Template
{
    /** @var array */
    protected $customerData = array();
    /** @var Config */
    protected $configHelper;
    /** @var  CustomerSession */
    protected $customerSession;
    /** @var  CheckoutSession\ */
    protected $checkoutSession;
    /** @var  Data */
    protected $dataHelper;
    /** @var  Product */
    protected $product;
    /** @var  Image */
    protected $imageHelper;
    /** @var  CollectionFactory */
    protected $categoryCollectionFactory;
    /** @var  ProductMetadataInterface */
    protected $productMetadataInterface;
    /** @var  ModuleListInterface */
    protected $moduleListInterface;
    /** @var  Configurable */
    protected $configurable;
    /** @var  Registry */
    protected $registry;
    /** @var  Subscriber */
    protected $subscriberHelper;
    /** @var  Order */
    protected $orderModel;
    /** @var  CountryFactory */
    protected $countryFactory;
    /** @var  EdroneIns */
    protected $edroneIns;
    /** @var  EdroneIns */
    protected $edroneEventOrder;

    public function __construct(
        Template\Context $context,
        array $data,
        Config $configHelper,
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        Data $dataHelper,
        Product $product,
        Image $imageHelper,
        CollectionFactory $categoryCollectionFactory,
        ProductMetadataInterface $productMetadataInterface,
        ModuleListInterface $moduleListInterface,
        Configurable $configurable,
        Registry $registry,
        Subscriber $subscriberHelper,
        Order $orderModel,
        CountryFactory $countryFactory,
        EdroneIns $edroneIns,
        EdroneEventOrder $edroneEventOrder
    ) {
        $this->configHelper    = $configHelper;
        $this->customerSession = $customerSession;
        $this->checkoutSession = $checkoutSession;
        $this->dataHelper      = $dataHelper;
        $this->product         = $product;
        $this->imageHelper     = $imageHelper;
        $this->categoryCollectionFactory  = $categoryCollectionFactory;
        $this->productMetadataInterface = $productMetadataInterface;
        $this->moduleListInterface = $moduleListInterface;
        $this->configurable = $configurable;
        $this->registry = $registry;
        $this->subscriberHelper = $subscriberHelper;
        $this->orderModel = $orderModel;
        $this->countryFactory = $countryFactory;
        $this->edroneIns = $edroneIns;
        $this->edroneEventOrder = $edroneEventOrder;
        parent::__construct($context, $data);
    }

    /**
     * @return Config
     */
    public function getConfigHelper()
    {
        return $this->configHelper;
    }
    /**
     * @return EdroneEventOrder
     */
    public function getEdroneEventOrder()
    {
        return $this->edroneEventOrder;
    }

    /**
     * @return Data
     */
    public function getDataHelper()
    {
        return $this->dataHelper;
    }

    public function getMagentoVersion()
    {
        return $this->productMetadataInterface->getVersion();
    }

    /**
     * @return array
     */
    public function getCustomerData()
    {
        if (!count($this->customerData)) {
            if ($this->customerSession->isLoggedIn()) {
                $this->getLoggedCustomerData();
            } else {
                $this->getGuestCustomerData();
            }
        }

        return $this->customerData;
    }

    private function getLoggedCustomerData()
    {
        $customer                         = $this->customerSession->getCustomer();
        $this->customerData['first_name'] = $customer->getFirstname();
        $this->customerData['last_name']  = $customer->getLastname();
        $this->customerData['email']      = $customer->getEmail();

        $subscriber = $this->subscriberHelper->loadByEmail($customer->getEmail());
        if ($subscriber) {
            $this->customerData['subscriber_status'] = $subscriber->isSubscribed() ? "1" : "";
        }

        if ($address = $customer->getDefaultShippingAddress()) {
            $this->customerData['country'] = $address->getCountry();
            $this->customerData['city']    = $address->getCity();
            $this->customerData['phone']   = $address->getTelephone();
        } else {
            $this->customerData['country'] = '';
            $this->customerData['city']    = '';
            $this->customerData['phone']   = '';
        }

        $this->customerData['is_logged_in'] = 1;
    }

    private function getGuestCustomerData()
    {
        $this->customerData['first_name'] = '';
        $this->customerData['last_name']  = '';
        $this->customerData['email']      = '';
        $this->customerData['country']    = '';
        $this->customerData['city']       = '';
        $this->customerData['phone']      = '';

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote   = $this->checkoutSession->getQuote();
        $address = $quote->getBillingAddress();

        if ($address) {
            $this->customerData['first_name'] = $address->getFirstname();
            $this->customerData['last_name']  = $address->getLastname();
            $this->customerData['email']      = $address->getEmail();
            $this->customerData['country']    = $address->getCountry();
            $this->customerData['city']       = $address->getCity();
            $this->customerData['phone']      = $address->getTelephone();
        }
        $this->customerData['subscriber_status'] = '';
        $this->customerData['is_logged_in']      = 0;
    }

    /**
     * @return Product
     */
    public function getProductModel()
    {
        return $this->product;
    }
    /**
     * @return Order
     */
    public function getOrderModel()
    {
        return $this->orderModel;
    }
    /**
     * @return CountryFactory
     */
    public function getCountryFactory()
    {
        return $this->countryFactory;
    }
    /**
     * @return EdroneIns
     */
    public function getEdroneIns()
    {
        return $this->edroneIns;
    }

    /**
     * @return Image
     */
    public function getImageHelper()
    {
        return $this->imageHelper;
    }

    /**
     * @return Configurable
     */
    public function getConfigurableModel()
    {
        return   $this->configurable;
    }

    public function getCategoryCollection($categoryIds)
    {
        return $this->categoryCollectionFactory->create()->addIdFilter($categoryIds)->addAttributeToSelect('name');
    }

    public function getWebsiteId()
    {
        return $this->_storeManager->getStore()->getWebsiteId();
    }

    public function getStoreId()
    {
        return $this->_storeManager->getStore()->getId();
    }

    public function getLocaleCode()
    {
        return $this->_scopeConfig->getValue(
            'general/locale/code',
            ScopeInterface::SCOPE_STORE,
            $this->_storeManager->getStore()
        );
    }

    public function getModuleVersion()
    {
        return $this->moduleListInterface->getOne('Edrone_Magento2module')['setup_version'];
    }

    public function getCheckoutSession()
    {
        return $this->checkoutSession;
    }

}
