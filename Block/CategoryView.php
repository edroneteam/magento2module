<?php
namespace Edrone\Magento2module\Block;

class CategoryView extends Magento2module
{

    /**
     * @return array
     */
    public function getCategoryData()
    {
        $category = $this->registry->registry('current_category');
        $categoryNames = [];
        $categoryIds = [];
        $parentCategoryIds = $category->getParentIds();
        foreach ($parentCategoryIds as $id) {
            $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $cat = $_objectManager->create('Magento\Catalog\Model\Category')->load($id);
            $categoryNames[] = $cat->getName();
            $categoryIds[] = $id;
        }
        $categoryIds[] = $category->getId();
        $categoryNames[] = $category->getName();

        $productArray['product_category_names'] = join('~', $categoryNames);
        $productArray['product_category_ids']   = join('~', $categoryIds);
        return $productArray;
    }
}
