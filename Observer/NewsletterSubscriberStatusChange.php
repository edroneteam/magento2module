<?php

namespace Edrone\Magento2module\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Edrone\Magento2module\Helper\EdroneIns;
use Edrone\Magento2module\Helper\EdroneEventSubscribe;


class NewsletterSubscriberStatusChange implements ObserverInterface {

    public function __construct(EdroneIns $edrone) {
        $this->edrone = $edrone;
    }

    public function execute(Observer $observer) {
        $subscriber = $observer->getEvent()->getSubscriber();

        if ($subscriber->getSubscriberStatus() == 3) {
            $subscriberStatus = 0;
        } elseif ($subscriber->getSubscriberStatus() == 1) {
            $subscriberStatus = 1;
        } elseif ($subscriber->getSubscriberStatus() == 2) {
            $subscriberStatus = "";
        } elseif ($subscriber->getSubscriberStatus() == 4) {
            $subscriberStatus = "";
        }
        if ($subscriber->isStatusChanged()) {
            try {
                $this->configureAndSendTrace($subscriberStatus, $subscriber->getEmail());
            } catch (\Exception $e) {
                $this->failedConfigureAndSendTraceErrorLog($e);
            }
        }
    }

    private function configureAndSendTrace($subscriberStatus, $subscriberEmail) {
        $edrone = $this->edrone;

        $edrone->setCallbacks(
                function ($obj) {
            error_log("EDRONEPHPSDK ERROR - wrong request:" . json_encode($obj->getLastRequest()));
        }
        );

        $edrone->prepare(
                EdroneEventSubscribe::create()->userEmail($subscriberEmail)->userSubscriberStatus($subscriberStatus)
        )->send();
    }

    private function failedConfigureAndSendTraceErrorLog($e) {
        error_log("EDRONEPHPSDK ERROR:" . $e->getMessage() . ' more :' . json_encode($e));
    }
}
