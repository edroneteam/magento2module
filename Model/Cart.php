<?php
namespace Edrone\Magento2module\Model;

use Magento\Framework\DataObject;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Edrone\Magento2module\Helper\ProductHelper as EdroneProductHelper;


class Cart extends DataObject{

    protected $productRepositoryInterface;
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;


    /**
     *
     * @var EdroneProductHelper
     */
    protected $edroneProductHelper;

    /**
     * Cart constructor.
     * @param CheckoutSession $checkoutSession
     * @param array $data
     */
    public function __construct(
        ProductRepositoryInterface $productRepositoryInterface,
        CheckoutSession $checkoutSession,
        EdroneProductHelper $edroneProductHelper,
        array $data = []
    ) {
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->checkoutSession = $checkoutSession;
        $this->edroneProductHelper = $edroneProductHelper;
    }

    public function getCart(){
        $last = $this->checkoutSession->getLastAddedProductId(true);
        if($last !== null){
            $product = $this->productRepositoryInterface->getById($last);
            return $this->edroneProductHelper->getProductData($product);
        }
        return false;
    }

}
