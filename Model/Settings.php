<?php
namespace Edrone\Magento2module\Model;

use Magento\Framework\DataObject;
use Edrone\Magento2module\Helper\Config as EdroneConfig;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\ModuleListInterface;

class Settings extends DataObject{

    /**
     *
     * @var EdroneConfig
     */
    protected $edroneConfig;
    /**
     *
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    protected $moduleListInterface;

    protected $productMetadataInterface;
    /**
     * Constructor
     * @param EdroneConfig $edroneConfig
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
            EdroneConfig $edroneConfig,
            StoreManagerInterface $storeManager,
            ScopeConfigInterface $scopeConfig,
            ModuleListInterface $moduleListInterface,
            ProductMetadataInterface $productMetadataInterface
            ){
        $this->storeManager = $storeManager;
        $this->edroneConfig = $edroneConfig;
        $this->scopeConfig  = $scopeConfig;
        $this->moduleListInterface  = $moduleListInterface;
        $this->productMetadataInterface  = $productMetadataInterface;
    }
    /**
     *
     * @return string
     */
    public function getMagentoVersion()
    {
        return $this->productMetadataInterface->getVersion();
    }
    /**
     *
     * @return integer
     */
    public function getWebsiteId()
    {
        return $this->storeManager->getStore()->getWebsiteId();
    }
    /**
     *
     * @return integer
     */
    public function getStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }
    /**
     *
     * @return string
     */
    public function getLocaleCode()
    {
        return $this->scopeConfig->getValue(
            'general/locale/code',
            ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }

    public function getModuleVersion()
    {
        return $this->moduleListInterface->getOne('Edrone_Magento2module')['setup_version'];
    }

    public function getAddToCartMode()
    {
        return $this->edroneConfig->isAddToCartEnabled();
    }


    public function getSettings(){
        return [
                'appid' => $this->edroneConfig->getAppId(),
                'shop_lang'=> $this->getLocaleCode(),
                'website_id'=> $this->getWebsiteId(),
                'shop_id'   => $this->getStoreId(),
                'version'   => $this->getModuleVersion(),
                'platform_version' => $this->getMagentoVersion(),
                'platform' => 'magento2',
                'add_to_cart' => $this->getAddToCartMode()
               ];
    }

}
