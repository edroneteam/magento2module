<?php
namespace Edrone\Magento2module\Model;
use Magento\Framework\DataObject;
use Edrone\Magento2module\Helper\ProductHelper as EdroneProductHelper;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Description of Order
 *
 * @author abialozyt
 */
class Order extends DataObject {

    /**
     * @var CollectionFactory
     */
    protected $_salesOrderCollection;

    protected $_orderCollection = null;

    /** @var \Magento\Store\Model\StoreManagerInterface */
    protected $_storeManager;

    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     *
     * @var EdroneProductHelper
     */
    protected $edroneProductHelper;
    /**
     *
     * @var ProductRepositoryInterface
     */
    protected $productRepositoryInterface;

    /**
     * Order constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface $salesOrderCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Escaper $escaper
     * @param array $data
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface $salesOrderCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Escaper $escaper,
        EdroneProductHelper $edroneProductHelper,
        ProductRepositoryInterface $productRepositoryInterface,
        array $data = []
    ) {
        parent::__construct($data);
        $this->_salesOrderCollection = $salesOrderCollection;
        $this->_storeManager = $storeManager;
        $this->_escaper = $escaper;
        $this->edroneProductHelper = $edroneProductHelper;
        $this->productRepositoryInterface = $productRepositoryInterface;
    }



    public function getOrder()
    {
        $collection = $this->getOrderCollection();

        if(!$collection){
            return false;
        }

        $data = [
            'products' => []
        ];

        /* @var \Magento\Sales\Model\Order $order */
        foreach ($collection as $order) {
            foreach ($order->getAllVisibleItems() as $item) {
                $product = $this->productRepositoryInterface->getById($item->getId());
                $data['products'][] = $this->edroneProductHelper->getProductData($product);
            }
        }

        return $data;
    }
}
