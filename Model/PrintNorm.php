<?php
namespace Edrone\Magento2module\Model;

/**
 * Description of PrintNorm
 *
 * @author abialozyt
 */
class PrintNorm {
    //put your code here
    /**
     * 
     * @param mixed $data
     */
    public static function norm($data){
        if(is_array($data)){
            foreach($data as $key => $value ){
                $data[$key] = self::norm($value);
            }
            return $data;
        }
        if(is_string($data)){
            return urlencode($data);
        }
        return $data;
    }
    
}
