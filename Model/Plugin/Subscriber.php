<?php

namespace Edrone\Magento2module\Model\Plugin;

use Edrone\Magento2module\Helper\EdroneIns;
use Edrone\Magento2module\Helper\EdroneEventSubscribe;
use Edrone\Magento2module\Helper\Config;

class Subscriber
{
    protected $configHelper;
    protected $_customer;
    protected $_customerSession;
    protected $_storeManager;

    public function __construct(
        Config $configHelper,
        EdroneIns $edrone,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customer,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->edrone           = $edrone;
        $this->configHelper    = $configHelper;
        $this->_customer        = $customer;
        $this->_customerSession = $customerSession;
        $this->_storeManager    = $storeManager;
        $this->_scopeConfig     = $context->getScopeConfig();
    }

    public function beforeUnsubscribeCustomerById(
        $subscriber,
        $customerId
    ) {
        if ($this->configHelper->getNewsletterSubscriptionMailEnabled()) {
            $subscriber->loadByCustomerId($customerId);

            if ($subscriber->isStatusChanged()) {
                try {
                    $this->configureAndSendTrace("0", $subscriber->getEmail());
                } catch (\Exception $e) {
                    $this->failedConfigureAndSendTraceErrorLog($e);
                }
            }
        }
        return [$customerId];
    }

    public function beforeSubscribeCustomerById(
        $subscriber,
        $customerId
    ) {
        /**
         * @var $subscriber \Magento\Newsletter\Model\Subscriber
         */
        if ($this->configHelper->getNewsletterSubscriptionMailEnabled()) {
            $subscriber->loadByCustomerId($customerId);
            if (!$subscriber->isSubscribed()) {

                $storeId = $subscriber->getStoreId();
                $isSubscribeOwnEmail = $this->_customerSession->isLoggedIn()
                    && $this->_customerSession->getCustomerDataObject()->getEmail() == $subscriber->getSubscriberEmail();
                if ($this->configHelper->getNewsletterSubscriptionMailEnabled($storeId) && !$isSubscribeOwnEmail) {
                    $status = "";
                } else {
                    $status = "1";
                }
                try {
                    $this->configureAndSendTrace($status, $subscriber->getEmail());
                } catch (\Exception $e) {
                    $this->failedConfigureAndSendTraceErrorLog($e);
                }
            }
        }
        return [$customerId];
    }

    public function beforeSubscribe(
        $subscriber,
        $email
    ) {
        if ($this->configHelper->getNewsletterSubscriptionMailEnabled()) {
            if (!$subscriber->isSubscribed()) {

                $storeId = $subscriber->getStoreId();
                if ($this->configHelper->getNewsletterSubscriptionMailEnabled($storeId)) {
                    $status = "";
                } else {
                    $status = "1";
                }
                try {
                    $this->configureAndSendTrace($status, $subscriber->getEmail());
                } catch (\Exception $e) {
                    $this->failedConfigureAndSendTraceErrorLog($e);
                }
            }
        }
        return [$email];
    }

    public function beforeUnsubscribe(
        \Magento\Newsletter\Model\Subscriber $subscriber
    ) {

        if ($this->configHelper->getNewsletterSubscriptionMailEnabled()) {
            try {
                $this->configureAndSendTrace("0", $subscriber->getSubscriberEmail());
            } catch (\Exception $e) {
                $this->failedConfigureAndSendTraceErrorLog($e);
            }
        }
        return null;
    }
    public function afterDelete(
        \Magento\Newsletter\Model\Subscriber $subscriber
    ) {

        if ($this->configHelper->getNewsletterSubscriptionMailEnabled()) {
            try {
                $this->configureAndSendTrace("0", $subscriber->getSubscriberEmail());
            } catch (\Exception $e) {
                $this->failedConfigureAndSendTraceErrorLog($e);
            }
        }
        return null;
    }

    public function aroundSendConfirmationRequestEmail(\Magento\Newsletter\Model\Subscriber $subject, callable $proceed) {
        if ($this->configHelper->getNewsletterSubscriptionMailEnabled()) {
            return null;
        }

        return $proceed();
    }

    private function configureAndSendTrace($subscriberStatus, $subscriberEmail) {
        $edrone = $this->edrone;

        $edrone->setCallbacks(
            function ($obj) {
                error_log("EDRONEPHPSDK ERROR - wrong request:" . json_encode($obj->getLastRequest()));
            }
        );

        $edrone->prepare(
            EdroneEventSubscribe::create()->userEmail($subscriberEmail)->userSubscriberStatus($subscriberStatus)
        )->send();
    }
}
