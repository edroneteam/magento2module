<?php
namespace Edrone\Magento2module\Model;


use Magento\Framework\DataObject;
use Magento\Newsletter\Model\Subscriber;
use Magento\Customer\Model\Session;

class Customer extends DataObject
{

    /**
     * Customer session
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;
    protected $_subscriber;

    /**
     * Customer constructor.
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        Session $customerSession,
        Subscriber $subscriber,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_subscriber = $subscriber;
        parent::__construct($data);
    }

    /**
     * Get Customer array
     *
     * @return array
     */
    public function getCustomer(){

         if($this->_customerSession->isLoggedIn()){

            $customer = $this->_customerSession->getCustomer();
            $subscriber = $this->_subscriber->loadByEmail($customer->getEmail());
            $field_subscriber_status = "";
            if ($subscriber) {
                $field_subscriber_status = $subscriber->isSubscribed() ? "1" : "";
            }

            if ($address = $customer->getDefaultShippingAddress()) {
                $field_country = $address->getCountry();
                $field_city    = $address->getCity();
                $field_phone   = $address->getTelephone();
            } else {
                $field_country = '';
                $field_city    = '';
                $field_phone   = '';
            }

            return [
                'isLoggedIn'=> $this->_customerSession->isLoggedIn(),
                'firstName' => $customer->getFirstname(),
                'lastName'  => $customer->getLastname(),
                'email'     => $customer->getEmail(),
                'phone'     => $field_phone,
                'city'      => $field_city,
                'country'     => $field_country,
                'subscriber_status' => $field_subscriber_status
            ];
        }
        else{
            return [
                'isLoggedIn' => $this->_customerSession->isLoggedIn(),
            ];
        }

    }

}
