<?php

/**
 * Created by Q-Solutions Studio
 * Date: 26.01.17
 *
 * Modified by Edrone Team
 * Date: 23.05.18
 *
 * @category    Edrone
 * @package     Edrone_Magento2module
 * @author      Lukasz Owczarczuk <lukasz@qsolutionsstudio.com>
 */

namespace Edrone\Magento2module\Plugin;

use Edrone\Magento2module\Helper\EdroneEventSubscribe;
use Edrone\Magento2module\Helper\EdroneIns;
use Magento\Newsletter\Model\Subscriber;

class NewsletterSubscriberChangePlugin {

    private $edrone;

    public function __construct(EdroneIns $edrone) {
        $this->edrone = $edrone;
    }

    // unsubscribe from admin panel
    public function afterUnsubscribe(Subscriber $subscriber, $result) {
        if (!$subscriber->isSubscribed()) {
            $subscriberStatus = 0;

            try {
                $this->configureAndSendTrace($subscriberStatus, $subscriber->getEmail());
            } catch (\Exception $e) {
                $this->failedConfigureAndSendTraceErrorLog($e);
            }
        }
        return $result;
    }

    private function configureAndSendTrace($subscriberStatus, $subscriberEmail) {
        $edrone = $this->edrone;

        $edrone->setCallbacks(
                function ($obj) {
            error_log("EDRONEPHPSDK ERROR - wrong request:" . json_encode($obj->getLastRequest()));
        }
        );

        $edrone->prepare(
                EdroneEventSubscribe::create()->userEmail($subscriberEmail)->userSubscriberStatus($subscriberStatus)
        )->send();
    }

    private function failedConfigureAndSendTraceErrorLog($e) {
        error_log("EDRONEPHPSDK ERROR:" . $e->getMessage() . ' more :' . json_encode($e));
    }

}
